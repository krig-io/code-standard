# JavaScript/EcmaScript coding standard rules

This file intends to give information about the current standard used in JS/ES code in KRIG projects.

As of now, the KRIG standard follows the [JavaScript semistandard](https://github.com/Flet/semistandard) which in short is the [standard](https://github.com/standard/standard) style but with semicolons.  

Install the semistandard with npm or yarn in the project directory with:

```bash
npm install semistandard
yarn add semistandard
```

It is recommended that you install the semistandard into your IDE or code editor to get hints on invalid code.  

On a project basis the semistandard should be installed as a dev dependency and be run on either `npm test` or `yarn test` (depending on package manager).
