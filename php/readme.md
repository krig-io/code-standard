# PHP coding standard rules

This file contains the style rules for general PHP code.  
If anything is missing, refer to the PHP psr2 or psr4 coding standard.

The key words **must**, **must not**, **required**, **shall**, **shall not**, **should**, **should not**, **recommended**, **may**, and **optional** in this document are to be interpreted as described in [RFC 2119](http://www.ietf.org/rfc/rfc2119.txt).

## 1. Overview

* Php code **must** use only `<?php` tag.
* Files **must** use UTF-8 without BOM.
* Namespaces, Interfaces, Traits and Classes **must** follow the [PSR-4](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader.md) autoloading standard.
* Namespaces, Interfaces, Traits and classes names **must** be declared in `PascalCase`.
* Class constants **must** be declared in all upper case with underscore separators.
* Method and Function names **must** be declared in `camelCase`.

## 2. Files

### 2.1 General file rules

PHP code **must** only use the `<?php` open tag and **should not** use the `<?=` short tag.  
All files **must** end with a single blank line.  
The closing tag (`?>`) **must** be omitted from files containing only php.  
Files **must** use the standard unix `lf` line endings.  

A file **should** either declare new symbols (classes, functions, interfaces etc) and cause no other side effects or it **should** execute logic with side effects. A file **should not** do both.

The following is an example from the PSR-1 standard explaining difference between declaration and side effects.

```php
<?php
// side effect: change ini settings
ini_set('error_reporting', E_ALL);

// side effect: loads a file
include "file.php";

// side effect: generates output
echo "<html>\n";

// declaration
function foo()
{
  // function body
}
```

### 2.2 Lines

There **should not** be a hard limit on line length.  
The soft limit on line length **should** be 120 characters.  
Lines longer than 120 characters **should** be split into multiple subsequent lines of code no longer than 120 characters.  
There **must** be no trailing whitespace at the end of a line.  
Blank lines **may** be added to improve readability.  
There **may not** be more than one statement per line.  

### 2.3 Indenting

Code **must** use an indent of 4 spaces and **must not** use tab characters for indenting.  

### 2.4 Keywords

Keywords (`true`, `false`, `null`) **must** be in lower case.

## 3. Namespace, Interface, Trait and Class -names

*Interfaces, traits and classes are referred to as "class" in this context if nothing else is stated*  

### 3.1 General namespace, interface, trait and class -rules

All namespaces and class names **must** follow the [PSR-4](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader.md) autoloading standard.  
Each *class* **must** be declared in a file by itself.  
*Classes* should always be declared in `PascalCase` (aka `StudlyCaps`).  
Namespaces **must** always be declared on the first line after the starting php tag (`<?php`).  
Any `use` declarations **must** go after the namespace declaration with one empty line between.  
There **must** be a blank line after the `use` block.  
It's **recommended** to have one `use` keyword per declaration, but the code **may** contain multiple declarations in a single `use`.  

### 3.2 Extends and Implements

The `extends` and `implements` keywords **must** be declared on the same line as the class name.  
The opening tag of the class **should** go after the class declaration and **should not** go on a new line.  
The closing tag of the class **should** go on the next line after the body.  

### 3.3 Properties / Fields

Visibility (`public`, `private`, `protected`) **must** be declared on all properties.  
The `var` keyword **must not** be used.  
There **may not** be more than one field declared per statement.  
Field names **must** use `camelCase` names.  

### 3.4 Methods

Visibility **must** be declared for methods.  
Method names **must** use `camelCase` names.  
The opening brace of a method **should** go on the same line as its declaration.  

#### 3.4.1 Method arguments & return types

Arguments **must** be typehinted if possible.  
Return types **must** be typehinted if possible.  
Arguments and return types **should** be commented with php-docs comments if not type hinted with strong types.  

### 3.5 Abstract, final and static

When used, `abstract` and `final` declarations **must** precede the visibility declaration or the `class` keyword.  
Any `static` declaration **must** come after the visibility declaration.  

### 3.6 Method and Function calls.  

When making a method or function call, there **must not** be a space between the name and the parenthesis.  
There **may not** be a space before the closing parenthesis.  
There **shall not** be a space before each comma but there **shall** be a space before the argument if its preceded by a comma.  
Argument lists **may** be split across multiple lines. Each line should be indented so that the arguments are aligned.  

### 3.7 Constants

Constants **must** be declared in upper case with underscore separators.  

Example:

```php
class {
  const A_CONSTANT    = "foo";
  const ANOTHER_CONST = 42;
}
```

### 3.8 Examples

Example of a class file using the above rules: 

```php
<?php
namespace Vendor\Foo;

use Vendor\Stuff;    // Preferred.
use AnotherVendor { // Discouraged but allowed.
  \TheClass,
  \TheInterface
}

abstract class Bar extends TheClass implements TheInterface {
  using Stuff; // Trait declaration right after the class declaration line.

  private $aField       = null;
  public  $anotherFiled = 'foo';

  public function __construct(int $arg1, string $arg2 = null) {
    $this->aField       = $arg1;
    $this->anotherField = $arg2;
  }

  public function getField() : ?int {
    return $this->aField;
  }

  /**
  * @property string|object|null $arg
  */
  public function setAnotherField($arg) {
    $this->anotherField = $arg;
  }

}
```

## 4 Control Structures

General rules of thumb:  
  
* There **must** be one space after the keyword.
* There **must not** be a space after the opening parenthesis.
* There **must not** be a space before the closing parenthesis.
* There **must** be one space between the closing parenthesis and the opening brace.
* The structure body **must** be indented once.
* The closing brace **should** be on the next line after the body.
* The opening brace **must** be on the same line as the keyword.
* Braces **must** be used.
* There **must** be one space after a control structure.

### 4.1 if / else

The keyword `elseif` **should not** be used.

Example of legal if clauses:
*Note the spacing.*

```php
if ($a === $b) {
  return 3;
} else if ($a === $c) {
  return 2;
}
```

### 4.2 switch / case

The `case` keyword **must** be indented once, while its body should be indented another step.  
Braces could **optionally** be used for the case body.  
Fall-through **may** be noted with a comment (as per `PSR-2` the `// no break` comment is preferred but not forced).  
A `default` case **should** be implemented, if so by only throwing an exception.  

Example of legal switch clause:

```php
switch ($expression) {
    case 0: 
        return 1;
    case 1: // no break
    case 2: {
        return 42;
    }
    default:
        throw new Exception('Foo');
}
```

### 4.3 while, do while

Example of legal while and do while:

```php
while ($expr) {
  // Code
}

do {
  // Code
} while ($expr);
```

### 4.4 for and foreach

It´s **recommended** to use the array methods either in the language or as a external package rather than the `for` or `foreach` methods.  
But when using them, which is perfectly fine, the following example is an example of legal usages:

```php
for ($index = 0; $index < $count; $index++) {
  // Body
}

foreach ($collection as $key => $value) { // Note that fetching both the key and the value is not required.
  // Body
}
```

### 4.5 try / catch / finally

Try and Catch **should** be used in cases where exceptions is supposed to be caught.  
When cleaning up resources after exception catching, a finally clause is **recommended**.  
When possible, it is **recommended** catch more than one exception to make the possible output more specific.

Example of legal try/catch clauses:

```php
try {
  // Try something
} catch (IOException $ex) {
  return 0;
} catch (Exception $ex) {
  return 1;
} finally {
  $this->cleanUp();
}
```

## 5 Closures

Closures mainly follow the same guidelines as methods and functions.  
The `use` keyword should be both appended and prepended by a space. Values inside the `use` keyword follows the rules that function arguments follow.  

Example of a legal closure declaration:

```php
$closure = function(string $arg1, ?int $arg2) use (string $arg3) : bool {
  // Body
};
```

## 6 Strong typing and type-hints

When applicable, strong types and/or type-hint in comment form **should** be used.  
If php 7+, this includes `return` values.  
If php 7.2+, this includes `void` return value.

Type name **should not** be prepended by a space if not after a comma, but should always have a space between itself and its variable name.  
The `:` (colon) of a return type **should not** have a space between the ending parenthesis and the type name.  
The type name **should** have a space between itself and the method open brace.

Example of a function with typing:

```php
// Php 5.x

/**
* @param string $arg1
* @param string|int $arg2
* @param int|null $arg3
* @return string
*/
public function aFunction(string $arg1, $arg2, $arg3 = null) {
  return "foo";
}


// Php 7.x

/**
* @param string $arg1
* @param string|int $arg2
* @param int|null $arg3
* @return string
*/
public function aFunction(string $arg1, $arg2, ?int $arg3 = null): string {
  return "foo";
}
```

## 7. Strings

Strings **must not** use double quotations (`""`) with a few exceptions (regular expressions for example).  
In case the string have no parameters, single quotations should be used (`''`), in case of parameterized strings, `printf` and `sprintf` **must** be used.  
Strings using single quotes may use dot notation to concatenate strings in case it requires multiple lines and some special cases may use dot notation to concatenate strings, but is not recommended.  
If dot notation is used to split lines, the dot should be either on the end of the line with a space before or on the next line aligned with other dots or the equal sign.

Valid examples:

```php
$str = 'A string without parameters'.

$str2 = sprintf('A string %s parameters.', 'with');

$str3 = 'A long string ...//...' .
        'with concatenation...';
```

## 8. Get and Set

To keep a clean and maintainable code, properties which are not `const` **must not** be public, but **must** use get and set methods.  
Get and Set methods **may not** do anything other than *returning* alt *setting* the value, that is, no other side effects is allowed.  

Example:

```php
class MyClass {
  public const MY_VARIABLE;

  private $myValue;

  public function getValue(): Type {
    return $this->value;
  }

  public function setValue(Type $value): void {
    $this->value = $value;
  }

}
```
